{{{$countAddedMembersMESSAGE or ''}}}

{{Form::open(array('url' => '/test', 'files' => true))}}
{{Form::file('file')}}
{{Form::submit('Upload')}}
{{Form::close()}}

<div class="members">
    @if($members->isEmpty())
    <p>Nuk ka anetare te shtuar ende</p>
    @else
    <table class="datatable">
        <thead class="theader">
        <tr>
            <th>Emri</th>
            <th>Mbiemri</th>
            <th>Telefoni</th>
            <th>Profesioni</th>
            <th>Veprimet</th>
        </tr>
        </thead>
        <tbody>
        @foreach($members as $member)
        <tr>
            <td>{{$member->emri}}</td>
            <td>{{$member->mbiemri}}</td>
            <td>{{$member->telefoni}}</td>
            <td>{{$member->profesioni}}</td>
            <td><a href="#">EDITO</a></td>
        </tr>
        @endforeach
        </tbody>
    </table>
    @endif
</div>