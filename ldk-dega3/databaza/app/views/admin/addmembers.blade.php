@extends('admin.layout')

@section('form')
    <div class="alert-box success">{{{ Session::get('addMemberConfirmation')}}}</div>
    @if(Auth::user()->username == 'admin')
        <script type="text/javascript">
            $(function() {
                $("[name=membersave]").click(function(){
                    var buttons = $("[name=membersave]");
                    if(buttons[0].checked){
                        $("#batch").hide();//('display', 'none');
                        $("#individual").show();
                    }
                    else{
                        $("#individual").hide();//css('display', 'none');
                        $("#batch").show();
                    }
                });
            });
        </script>
        <div>
            Selekto menyren si deshiron te shtosh anetare:<br>
            {{Form::open(array('url' => '#'))}}
            Nje anetare{{Form::radio('membersave', 'individual', true)}}
            Me excel{{Form::radio('membersave', 'batch')}}
            {{Form::close()}}
        </div>
    @endif
    <div class="form">
        {{Form::open(array('url' => 'add/member', 'id' => 'individual'))}}
        <div class="label">{{Form::label('firstname', 'Emri')}}</div><div class="input">{{Form::text('firstname', $member->emri)}}<span style="font-size: 10px; color: red">{{{$errors->first('firstname')}}}</span></div>
        <div class="label">{{Form::label('lastname', 'Mbiemri')}}</div><div class="input">{{Form::text('lastname', $member->mbiemri)}}<span style="font-size: 10px; color: red">{{{$errors->first('lastname')}}}</span></div>
        <div class="label">{{Form::label('birthday', 'Ditëlindja')}}</div><div class="input"><input name="birthday" type="text" id="datepicker" class="hasDatePicker" value="{{$member->datelindja}}"><span style="font-size: 10px; color: red">{{{$errors->first('birthday')}}}</span></div>
        <div class="label">{{Form::label('personalid', 'Numri personal')}}</div><div class="input">{{Form::text('personalid', $member->num_personal)}}<span style="font-size: 10px; color: red">{{{$errors->first('personalid')}}}</span></div>
        <div class="label">{{Form::label('nationality', 'Kombësia')}}</div><div class="input">{{Form::text('nationality', $member->kombesia)}}<span style="font-size: 10px; color: red">{{{$errors->first('nationality')}}}</span></div>
        <div class="label">{{Form::label('bloodtype', 'Grupi i gjakut')}}</div><div class="input">{{Form::select('bloodtype', array('' => '', '0+' => '0+', '0-' => '0-', 'A+' => 'A+', 'A-' => 'A-', 'B+' => 'B+', 'B-' => 'B-', 'AB+' => 'AB+', 'AB-' => 'AB-'), $member->grupi_gjakut)}}<span style="font-size: 10px; color: red">{{{$errors->first('bloodtype')}}}</span></div>
        <div class="label">{{Form::label('address', 'Adresa')}}</div><div class="input">{{Form::textarea('address', $member->adresa)}}<span style="font-size: 10px; color: red">{{{$errors->first('address')}}}</span></div>
        <div class="label">{{Form::label('gender', 'Gjinia')}}</div><div class="input">{{Form::select('gender', array('mashkull' => 'Mashkull', 'femer' => 'Femër'), $member->gjinia)}}<span style="font-size: 10px; color: red">{{{$errors->first('gender')}}}</span></div>

        <div class="label">{{Form::label('profession', 'Profesioni')}}</div><div class="input">{{Form::text('profession', $member->profesioni)}}<span style="font-size: 10px; color: red">{{{$errors->first('profession')}}}</span></div>
        <div class="label">{{Form::label('employer', 'Punëdhënësi')}}</div><div class="input">{{Form::text('employer', $member->vendi_punes)}}<span style="font-size: 10px; color: red">{{{$errors->first('employer')}}}</span></div>
        <div class="label">{{Form::label('phone', 'Telefoni')}}</div><div class="input">{{Form::text('phone', $member->telefoni)}}<span style="font-size: 10px; color: red">{{{$errors->first('phone')}}}</span></div>
        <div class="label">{{Form::label('branch', 'Dega')}}</div><div class="input">{{Form::text('branch', $member->dega)}}<span style="font-size: 10px; color: red">{{{$errors->first('branch')}}}</span></div>
        <div class="label">{{Form::label('subbranch', 'Nëndega')}}</div><div class="input">{{Form::text('subbranch', $member->nendega)}}<span style="font-size: 10px; color: red">{{{$errors->first('subbranch')}}}</span></div>

        <div class="label">{{Form::label('status', 'Statusi martesor')}}</div><div class="input">{{Form::select('status', array('i/e martuar' => 'I/e martuar', 'i/e pamartuar' => 'I/e pamartuar'))}}<span style="font-size: 10px; color: red">{{{$errors->first('status')}}}</span></div>
        <div class="label">{{Form::label('employed', 'I punësuar')}}</div><div class="input">{{Form::select('employed', array('Po' => 'Po', 'Jo' => 'Jo'), $member->i_punesuar)}}<span style="font-size: 10px; color: red">{{{$errors->first('employed')}}}</span></div>
        <div class="label">{{Form::label('email', 'E-mail')}}</div><div class="input">{{Form::email('email', $member->e_mail)}}<span style="font-size: 10px; color: red">{{{$errors->first('email')}}}</span></div>
        {{Form::submit('Shto anëtarin', array('class' => 'submit'))}}
        {{Form::close()}}

        @if(Auth::user()->username == 'admin')
        {{Form::open(array('url' => 'batch/members', 'id' => 'batch', 'style' => 'display: none;', 'files' => true))}}
        {{Form::file('file')}}
        {{Form::submit('Upload', array('class' => 'submit'))}}
        {{Form::close()}}
        @endif
    </div>
@stop