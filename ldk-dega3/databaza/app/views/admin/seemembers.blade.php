@extends('admin.layout')
@section('search-form')

@if(!$members->isEmpty())
    <div class="alert-box success">{{{Session::get('addMemberConfirmation')}}}</div>
    <div class="alert-box error">{{{$batchMembersError or ''}}}</div>
    <div style="float: left;">
        {{Form::open(array('url' => 'get/members', 'method' => 'GET'))}}
        <div style="float: left; margin: 5px 5px 5px 5px;">
            <div>{{Form::label('firstname', 'Emri')}}</div><div>{{Form::text('firstname')}}</div>
            <div>{{Form::label('lastname', 'Mbiemri')}}</div><div>{{Form::text('lastname')}}</div>
            <div>{{Form::label('profession', 'Profesioni')}}</div><div>{{Form::text('profession')}}</div>
            <div>{{Form::label('employer', 'Punëdhënësi')}}</div><div>{{Form::text('employer')}}</div>
            <div>{{Form::label('leftend', 'Datëlindja fillim')}}</div><div>{{Form::text('leftend', null, array('class' => 'hasDatePicker'))}}</div>
            <div>{{Form::label('phone', 'Telefoni')}}</div><div>{{Form::text('phone')}}</div>
        </div>
        <div style="float: right; margin: 5px 5px 5px 5px;">
            <div>{{Form::label('gender', 'Gjinia')}}</div>
            <div style="margin-top: 1px;">{{Form::select('gender', array('' => '', 'femer' => 'Femër', 'mashkull' => 'Mashkull'))}}</div>
            <div style="margin-top: 1px;">{{Form::label('bloodtype', 'Grupi i gjakut')}}</div>
            <div style="margin-top: 1px;">{{Form::select('bloodtype', array('' => '', '0+' => '0+', '0-' => '0-', 'A+' => 'A+', 'A-' => 'A-', 'B+' => 'B+', 'B-' => 'B-', 'AB+' => 'AB+', 'AB-' => 'AB-'))}}</div>
            <div style="margin-top: 1px;">{{Form::label('employed', 'I punësuar')}}</div><div style="margin-top: 1px;">{{Form::select('employed', array('' => '', 'Po' => 'Po', 'Jo' => 'Jo'))}}</div>
            <div style="margin-top: 1px;">{{Form::label('status', 'Statusi martesor')}}</div>
            <div style="margin-top: 1px;">{{Form::select('status', array('' => '', 'i/e martuar' => 'I/e martuar', 'i/e pamartuar' => 'I/e pamartuar'))}}</div>
            <div style="margin-top: 1px;">{{Form::label('email', 'E-mail')}}</div>
            <div>{{Form::email('email')}}</div>
        </div>
     	<div style="float: right; margin: 5px 5px 5px 5px;">
            <div>{{Form::label('ethnicity', 'Kombësia')}}</div><div>{{Form::text('ethnicity')}}</div>
            <div>{{Form::label('personalid', 'Numri personal')}}</div><div>{{Form::text('personalid')}}</div> 
            <div>{{Form::label('branch', 'Dega')}}</div><div>{{Form::text('Branch')}}</div>
            <div>{{Form::label('subbranch', 'Nëndega')}}</div><div>{{Form::email('subbranch')}}</div>
            <div>{{Form::label('rightend', 'Datëlindja mbarim')}}</div><div>{{Form::text('rightend', null, array('class' => 'hasDatePicker'))}}</div>
            {{Form::button('Kerko anetaret', array('class' => 'submit', 'id' => 'searchForm'))}}
        </div>
        {{Form::close()}}
    </div>
@endif
@stop
@section('members')
<script rtpe="text/javascript">
    var myTable;
    $(document).ready(function(){
        myTable = $('#dataTable').dataTable( {
            "bPaginate": true,
            "bStateSave" : false,
            "bServerSide": true,
            "bSort": false,
            "bFilter" : false,
            "bAutoWidth": true,
            "sAjaxSource": "<?php echo Request::root().'/get/members'; ?>",
            "aoColumns": [
                {"mDataProp": "emri", "aTargets":[1]},
                {"mDataProp": "mbiemri", "aTargets":[2]},
                {"mDataProp": "telefoni", "aTargets":[10]},
                {"mDataProp": "e_mail", "aTargets":[14]},
                {"mDataProp": "grupi_gjakut", "aTargets":[5]},
                {"mDataProp": "profesioni", "aTargets":[9]},
                {"mDataProp": "vendi_punes", "aTargets":[17]},
                {"mDataProp": "id", "aTargets":""}
            ],
            "fnCreatedRow": function(nRow, aData, iDataIndex){
                $('td:eq(7)', nRow).html("<a href={{URL::to('see/members')}}/" + aData.id + ">Edito</a> | <a href={{URL::to('delete/member')}}/" + aData.id + ">Fshij</a>");
            },
            "fnServerData": function(sSource, aoData, fnCallBack){
                aoData.push({"name" : "firstname", "value" : $("#firstname").val()});
                aoData.push({"name" : "lastname", "value" : $("#lastname").val()});
                aoData.push({"name" : "personalid", "value" : $("#personalid").val()});
                aoData.push({"name" : "email", "value" : $("#email").val()});
                aoData.push({"name" : "phone", "value" : $("#phone").val()});
                aoData.push({"name" : "employer", "value" : $("#employer").val()});
                aoData.push({"name" : "profession", "value" : $("#profession").val()});
                aoData.push({"name" : "bloodtype", "value" : $("#bloodtype").val()});
                aoData.push({"name" : "gender", "value" : $("#gender").val()});
                aoData.push({"name" : "ethnicity", "value" : $("#ethnicity").val()});
                aoData.push({"name" : "branch", "value" : $("#branch").val()});
                aoData.push({"name" : "subbranch", "value" : $("#subbranch").val()});
                aoData.push({"name" : "status", "value" : $("#status").val()});
                aoData.push({"name" : "employed", "value" : $("#employed").val()});
                aoData.push({"name" : "leftend", "value" : $("#leftend").val()});
                aoData.push({"name" : "rightend", "value" : $("#rightend").val()});
                $.getJSON(sSource, aoData, function(json){
                    fnCallBack(json);
                });
            }
        } );
	
        $("#searchForm").click(function(){
            myTable.fnReloadAjax();
        });
    });

</script>
    <div class="members" style="width: 150%;margin-top: 10px;">
        <div style="display: block">
            <input type="button" class="addmember" onclick="window.location='{{ URL::to('add/member');}}';" value="Shto anetare" />
            <a href="{{Url::to('download')}}">Shkarko anëtarët</a>
        </div>
        @if($members->isEmpty())
            <div class="alert-box notice" style="width: 250px;">Nuk ka anëtarë të shtuar ende</div>
        @else
            <table id="dataTable" class="dataTable">
                <thead>
                    <tr>
                        <th>Emri</th>
                        <th>Mbiemri</th>
                        <th>Telefoni</th>
                        <th>E-mail</th>
                        <th>Grupi gjakut</th>
                        <th>Profesioni</th>
                        <th>Punëdhënësi</th>
                        <th>Veprimet</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        @endif
    </div>
@stop