@section('search-form')
<div class="form">
    {{Form::open(array('url' => 'search/members'))}}
    <div class="label">{{Form::label('firstname', 'Emri')}}</div><div class="input">{{Form::text('firstname', $member->emri)}}</div>
    <div class="label">{{Form::label('lastname', 'Mbiemri')}}</div><div class="input">{{Form::text('lastname', $member->mbiemri)}}</div>
    <div class="label">{{Form::label('personalid', 'Numri personal')}}</div><div class="input">{{Form::text('personalid', $member->num_personal)}}</div>
    <div class="label">{{Form::label('bloodtype', 'Grupi i gjakut')}}</div><div class="input">{{Form::select('bloodtype', array('0', 'A', 'B', 'AB'))}}</div>
    <div class="label">{{Form::label('profession', 'Profesioni')}}</div><div class="input">{{Form::text('profession', $member->profesioni)}}</div>
    <div class="label">{{Form::label('employer', 'Punëdhënësi')}}</div><div class="input">{{Form::text('employer', $member->vendi_punes)}}</div>
    <div class="label">{{Form::label('phone', 'Telefoni')}}</div><div class="input">{{Form::text('phone', $member->telefoni)}}</div>
    <div class="label">{{Form::label('email', 'E-mail')}}</div><div class="input">{{Form::email('email', $member->e_mail)}}</div>

    {{Form::submit('Shto anëtarin', array('class' => 'submit'))}}
    {{Form:close()}}
</div>
@stop