<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Ndryshimi i passwordit</h2>

		<div>
			Për të ndryshuar passwordin kompletoni këtë forme: {{ URL::to('password/reset', array($token)) }}.
		</div>
	</body>
</html>