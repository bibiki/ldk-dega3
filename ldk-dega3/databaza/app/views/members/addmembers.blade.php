@extends('members.layout')

@section('form')
    <div class="alert-box success">{{{ Session::get('addMemberConfirmation')}}}</div>
    <div class="form">
        {{Form::open(array('url' => 'add/member'))}}
        <div class="label">{{Form::label('firstname', 'Emri')}}</div><div class="input">{{Form::text('firstname')}}<span style="font-size: 10px; color: red">{{{$errors->first('firstname')}}}</span></div>
        <div class="label">{{Form::label('lastname', 'Mbiemri')}}</div><div class="input">{{Form::text('lastname')}}<span style="font-size: 10px; color: red">{{{$errors->first('lastname')}}}</span></div>
        <div class="label">{{Form::label('birthday', 'Ditëlindja')}}</div><div class="input"><input name="birthday" type="text" id="datepicker" class="hasDatePicker"><span style="font-size: 10px; color: red">{{{$errors->first('birthday')}}}</span></div>
        <div class="label">{{Form::label('personalid', 'Numri personal')}}</div><div class="input">{{Form::text('personalid')}}<span style="font-size: 10px; color: red">{{{$errors->first('personalid')}}}</span></div>
        <div class="label">{{Form::label('nationality', 'Kombësia')}}</div><div class="input">{{Form::text('nationality')}}<span style="font-size: 10px; color: red">{{{$errors->first('nationality')}}}</span></div>
        <div class="label">{{Form::label('bloodtype', 'Grupi i gjakut')}}</div><div class="input">{{Form::select('bloodtype', array('' => '', '0+' => '0+', '0-' => '0-', 'A+' => 'A+', 'A-' => 'A-', 'B+' => 'B+', 'B-' => 'B-', 'AB+' => 'AB+', 'AB-' => 'AB-'))}}<span style="font-size: 10px; color: red">{{{$errors->first('bloodtype')}}}</span></div>
        <div class="label">{{Form::label('address', 'Adresa')}}</div><div class="input">{{Form::textarea('address')}}<span style="font-size: 10px; color: red">{{{$errors->first('address')}}}</span></div>
        <div class="label">{{Form::label('gender', 'Gjinia')}}</div><div class="input">{{Form::select('gender', array('mashkull' => 'Mashkull', 'femer' => 'Femer'))}}<span style="font-size: 10px; color: red">{{{$errors->first('gender')}}}</span></div>

        <div class="label">{{Form::label('profession', 'Profesioni')}}</div><div class="input">{{Form::text('profession')}}<span style="font-size: 10px; color: red">{{{$errors->first('profession')}}}</span></div>
        <div class="label">{{Form::label('employer', 'Punëdhënësi')}}</div><div class="input">{{Form::text('employer')}}<span style="font-size: 10px; color: red">{{{$errors->first('employer')}}}</span></div>
        <div class="label">{{Form::label('phone', 'Telefoni')}}</div><div class="input">{{Form::text('phone')}}<span style="font-size: 10px; color: red">{{{$errors->first('phone')}}}</span></div>
        <div class="label">{{Form::label('branch', 'Dega')}}</div><div class="input">{{Form::text('branch')}}<span style="font-size: 10px; color: red">{{{$errors->first('branch')}}}</span></div>
        <div class="label">{{Form::label('subbranch', 'Nëndega')}}</div><div class="input">{{Form::text('subbranch')}}<span style="font-size: 10px; color: red">{{{$errors->first('subbranch')}}}</span></div>

        <div class="label">{{Form::label('status', 'Statusi martesor')}}</div><div class="input">{{Form::select('status', array('i/e martuar' => 'I/e martuar', 'i/e pamartuar' => 'I/e pamartuar'))}}<span style="font-size: 10px; color: red">{{{$errors->first('status')}}}</span></div>
        <div class="label">{{Form::label('employed', 'I punësuar')}}</div><div class="input">{{Form::select('employed', array('Po' => 'Po', 'Jo' => 'Jo'))}}<span style="font-size: 10px; color: red">{{{$errors->first('employed')}}}</span></div>
        <div class="label">{{Form::label('email', 'E-mail')}}</div><div class="input">{{Form::email('email')}}<span style="font-size: 10px; color: red">{{{$errors->first('email')}}}</span></div>

        {{Form::submit('Shtoje anëtarin', array('class' => 'submit'))}}
        {{Form::close()}}
    </div>
@stop