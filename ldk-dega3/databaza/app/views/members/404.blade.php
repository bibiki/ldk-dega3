@extends('members.layout')

@section('404')
<div class="alert-box notice">Faqja që u përpoqët ta shihni nuk ekziston. Shfrytëzoni linkët sipër për navigim.</div>
@stop