<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    {{HTML::script('/js/jquery.js')}}
    {{HTML::script('/js/cufon-yui.js')}}

    {{HTML::script('/js/arial.js')}}
    {{HTML::script('/js/cuf_run.js')}}
    {{HTML::script('/js/jquery-ui-1.10.4.custom.min.js')}}
    {{HTML::script('/js/jquery-1.10.2.js')}}

    {{HTML::script('/js/jquery.datatables.js')}}
    {{HTML::script('/js/jquery.datatables.fnReloadAjax.js')}}

    {{HTML::script('/jquery-ui-1.10.4.custom/development-bundle/ui/jquery.ui.core.js')}}
    {{HTML::script('/jquery-ui-1.10.4.custom/development-bundle/ui/jquery.ui.widget.js')}}
    {{HTML::script('/jquery-ui-1.10.4.custom/development-bundle/ui/jquery.ui.datepicker.js')}}

    {{HTML::style('/css/style.css')}}
    {{HTML::style('/css/box.css')}}
    {{HTML::style('/DataTables-1.9.4/media/css/jquery.dataTables.css')}}
    {{HTML::style('/DataTables-1.9.4/media/css/demo_table_jui.css')}}

    {{HTML::style('/jquery-ui-1.10.4.custom/development-bundle/themes/base/jquery.ui.all.css')}}
    {{HTML::style('/jquery-ui-1.10.4.custom/development-bundle/demos/demos.css')}}
    <!-- CuFon: Enables smooth pretty custom font rendering. 100% SEO friendly. To disable, remove this section -->
    <script type="text/javascript">
        $(function() {
            $( "#datepicker" ).datepicker({changeMonth: true, changeYear:true, yearRange: "1900:2014", dateFormat:"yy/mm/dd"});
            $(".alert-box").each(function(){
                if($(this).text() === '')
                    $(this).hide();
            });
        });
    </script>
</head>
<body>
<div class="main">
    <div class="header">
        <div class="header_resize">
            <div class="clr"></div>
            <div class="menu_nav">
                <ul>
                    <li>{{HTML::link('/', 'Ballina')}}</li>
                    <li>{{HTML::link('//www.ldk-dega3.eu', 'Dega')}}</li>
                    <!--<li><a href="support.html">Support</a></li>
                    <li><a href="about.html">About Us</a></li>
                    <li><a href="blog.html">Blog</a></li>
                    <li><a href="contact.html">Contact Us</a></li>-->
                </ul>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="content_resize">
            <div class="mainbar">
                @yield('404')
                @yield('form')
            </div>
            <div class="clr"></div>
        </div>
    </div>

    <div class="fbg">
        <div class="footer">
            <div class="footer_resize">
                <p class="lf">&copy; Copyright MyWebSite. Designed by Blue <a href="http://www.bluewebtemplates.com">Website Templates</a></p>
                <ul class="fmenu">
                    <li>{{HTML::link('//', 'Ballina')}}</li>
                    <li>{{HTML::link('/login', 'Admin')}}</li>
                    @if(!Auth::check())
                    <li>{{HTML::link('remind', 'Password')}}</li>
                    @endif
                    <!--<li><a href="blog.html">Blog</a></li>
                    <li><a href="about.html">About Us</a></li>
                    <li><a href="contact.html">Contacts</a></li>-->
                </ul>
                <div class="clr"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>