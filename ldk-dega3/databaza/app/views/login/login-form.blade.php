@extends('members.layout')

@section('form')
    <div class="alert-box success">{{$logout or ''}}</div>
    <div class="alert-box error">{{$message or ''}}</div>
    <div class="form">
        {{Form::open(array('url' => 'login'))}}
        <div class="label">{{Form::label('username', 'Username')}}</div><div class="input">{{Form::text('username')}}</div>
        <div class="label">{{Form::label('password', 'Password')}}</div><div class="input">{{Form::password('password')}}</div>
        {{Form::image('images/submit.gif', null, array('class' => 'send'))}}
        {{Form::close()}}
    </div>
@stop