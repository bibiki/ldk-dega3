@extends('admin.layout')

@section('members')
<input class="addmember" type="button" onclick="window.location='{{ URL::to('add/user');}}';" value="Shto admin" />
<div class="members">
    @if($users->isEmpty())
        <p>Nuk ka admina të shtuar</p>
    @endif
    <table id="dataTable" class="dataTable" style="width: 100px; float: left;">
        <thead>
        <tr>
            <th>E-mail</th>
            <th>Username</th>
            <th>Aktiv</th>
            <th>Veprimet</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{$user->email}}</td>
            <td>{{$user->username}}</td>
            <td>
                @if($user->is_active)
                    Po
                @else
                    Jo
                @endif
            </td>
            <td style="white-space: nowrap;">{{HTML::link('see/users/'.$user->id, 'Edito')}} | {{HTML::link('delete/user/'.$user->id, 'Fshij')}}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@stop