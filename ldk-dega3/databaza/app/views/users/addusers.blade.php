@extends('admin.layout')

@section('form')
    <div class="alert-box success">{{{ $addUserConfirmation or ''}}}</div>
    <div class="alert-box error">{{{ $addUserError or ''}}}</div>
    <div class="form">
        @if(null == $user->email)
            {{Form::open(array('url' => 'add/user'))}}
            <div class="label">{{Form::label('email', 'Email')}}</div><div class="input">{{Form::email('email', $user->email)}}</div>
            <div class="label">{{Form::label('username', 'Username')}}</div><div class="input">{{Form::text('username', $user->username)}}</div>
        @else
            {{Form::open(array('url' => 'update/user'))}}
            <div class="label">{{Form::label('email', 'Email')}}</div><div class="input">{{$user->email}}</div>
            <div class="label">{{Form::label('username', 'Username')}}</div><div class="input">{{$user->username}}</div>
            {{Form::hidden('email', $user->email)}}
            {{Form::hidden('username', $user->username)}}
        @endif
        <div class="label">{{Form::label('password', 'Password')}}</div><div class="input">{{Form::password('password')}}</div>
        <div class="label">{{Form::label('password_confirm', 'Konfirmo passwordin')}}</div><div class="input">{{Form::password('password_confirm')}}</div>
        <div class="label">{{Form::label('is_active', 'Aktiv')}}</div><div class="input">{{Form::checkbox('is_active', 'po', $user->is_active)}}</div>
        @if(null == $user->email)
            <div class="label">{{Form::submit('Shto adminin', array('class' => 'submit'))}}</div>
        @else
            <div class="label">{{Form::submit('Edito adminin', array('class' => 'submit', 'style' => 'width: 163px;'))}}</div>
        @endif
        {{Form::close()}}
    </div>
@stop