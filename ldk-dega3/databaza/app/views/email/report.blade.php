<?php
/**
 * Created by JetBrains PhpStorm.
 * User: gagi
 * Date: 5/24/14
 * Time: 2:23 PM
 * To change this template use File | Settings | File Templates.
 */
echo "Përshëndetje Dega e Tretë e LDK-së, \"Dega e Presidentit Rugova\", Prishtinë,";

echo "Personi me emrin, ".$anetari->emri." ".$anetari->mbiemri.", u regjistrua";

echo "Për informata më të detajuara shikoni më poshtë:";

echo $anetari->emri." është i/e lindur më: ".$anetari->datelindja;

echo "Ai/ajo, ka grupin e gjakut:".$anetari->grupi_gjakut;

echo "Punon në: ".$anetari->vendi_punes;

echo "Me profesion është: ".$anetari->profesioni;

echo "Numri i telefonit të tij/saj është: ".$anetari->telefoni;