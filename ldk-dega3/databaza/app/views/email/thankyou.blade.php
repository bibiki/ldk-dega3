<?php
echo "Përshëndetje ".$anetari->emri.",<br><br>";
echo "Ju faleminderit që u bëtë anëtar i Degës së Tretë të LDK-së, 'Dega e Presidentit Rugova', Prishtinë<br><br>";

echo "Për çdo aktivitet të Degës do të informoheni me kohë.<br><br>";

echo "Sinqerisht,<br><br>";

echo "______________________________________________________<br><br>";
echo "Dega e Tretë e LDK-së, “Dega e Presidentit Rugova”, Prishtinë<br><br>";

echo "Adresa: Rruga “Hajrullah Abdullahu”, 2/a-1.- 1000 Prishtinë<br><br>";

echo "<b>Tel:</b> +377 44 116 201<br><br>";

echo "<b>E-posta:</b> info@ldk-dega3.eu<br><br>";

echo "<b>Ueb:</b> www.ldk-dega3.eu<br><br>";


echo "<b>Dega e Tretë në rrjetet sociale</b><br><br>";
echo "<a href='https://www.facebook.com/pages/Dega-III-e-LDK-s%C3%AB-Dega-e-Presidentit-Rugova-Prishtin%C3%AB/407846902631033'>Facebook</a><br><br>";

echo "<a href='https://twitter.com/DegaTrete'>Twitter</a><br><br>";

echo "<a href='https://plus.google.com/109917576591415398179/posts'>Google+</a><br><br>";

echo "<a href='https://www.youtube.com/channel/UCQnm3k67kI9LsP7v62597nA/feed?view_as=public'>Youtube</a><br><br>";
