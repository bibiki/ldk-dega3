@extends('members.layout')

@section('form')
    {{$error or ''}}</br>
    {{$status or ''}}
    <p>Forma për të kërkuar resetimin e passwordit:</p>
    <div class="form">
        {{Form::open(array('url' => 'remind'))}}
        <div class="label">{{Form::label('email', 'Email')}}</div><div class="input">{{Form::text('email')}}</div>
        {{Form::submit('Dërgo kërkesën', array('class' => 'send'))}}</div><div class="input">
        {{Form::close()}}
    </div>
@stop