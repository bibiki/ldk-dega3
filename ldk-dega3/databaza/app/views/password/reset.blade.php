@extends('members.layout')

@section('form')
    {{$error or ''}}
    <div class="form">
        {{Form::open(array('url' => 'reset'))}}
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="label">{{Form::label('email', 'Email')}}</div><div class="input">{{Form::email('email')}}</div>
        <div class="label">{{Form::label('password', 'Password')}}</div><div class="input">{{Form::password('password')}}</div>
        <div class="label">{{Form::label('password_confirmation', 'Konfirmo passwordin')}}</div><div class="input">{{Form::password('password_confirmation')}}</div>
        {{Form::submit('Ndrysho passwordin', array('class' => 'send', 'style' => 'width: 137px;'))}}
        {{Form::close()}}
    </div>
@stop
