<?php

	class UserController extends BaseController{
	
		public function add(){
			$view = View::make('users.addusers')->with('user', new User());
			if(isset($_POST['_token'])){
				$user = new User;
				$user->email = Input::get('email');
				$user->username = Input::get('username');
				$user->password = Hash::make(Input::get('password'));
				if(Input::get('password') != Input::get('password_confirm')){
                    $user->password = '';
                    $view = $view->with('user', $user);
                    return $view->with('addUserError', 'Passwordi i pa konfirmuar');
                }

                try{
                    $user->save();
                    if($user->id > 0)
                        $view = $view->with('addUserConfirmation', 'Useri u shtua me sukses');
                }
                catch(Exception $qe){
                    $user->password = '';
                    $view = $view->with('user', $user);
                    $view = $view->with('addUserError', 'Nje shfrytezues me ate e-mail ose username ekziston tashme.');
                    return $view;
                }
			}
			return $view;
		}

        public function changeOwnSettings(){

        }

        public function seeUsers($id = null){
            if(!$this::isAdmin()){
                Auth::logout();
                return View::make('404');
            }
            if(null != $id)
                return View::make('users.addusers')->with('user', User::find($id));
            $view = View::make('users.seeusers');
            $view = $view->with('users', User::all());
            return $view;
        }

        public function update(){
            if(!$this::isAdmin()){
                Auth::logout();
                return View::make('404');
            }
            $user = User::where('email', Input::get('email'))->get();
            $user = $user[0];
            if(null != $user){
                if(Input::get('password') != Input::get('password_confirm')){
                    $user->password = '';
                    $view = View::make('users.addusers');
                    $view = $view->with('user', $user);
                    return $view->with('addUserError', 'Passwordi i pakonfirmuar ose i zbrazet');
                }
                if(!str_is(Input::get('password'), ''))
                    $user->password = Hash::make(Input::get('password'));

                $user->is_active = str_is(Input::get('is_active'), 'po');
                try{
                    $user->save();
                    $view = View::make('users.seeusers')->with('users', User::all());
                    return $view;
                }
                catch(Exception $qe){
                    $user->password = '';
                    $view = $view->with('user', $user);
                    $view = $view->with('addUserError', 'Luteni te provoni serish te modifikoni administruesin ne fjale.');
                    return $view;
                }
            }
        }

        public function delete($id = null){
            if(!$this::isAdmin()){
                Auth::logout();
                return View::make('404');
            }
            if(null != $id)
                User::find($id)->get()[0]->delete();
            return View::make('users.seeusers')->with('users', User::all());
        }

        /*
         * I am putting this method related to member managment in UserController because
         * I do not want to duplicate isAdmin() method and I want to keep it in UserController.
         */
        public function batchMemberUpload(){
            if(!$this::isAdmin()){
                Auth::logout();
                return Redirect::to('/');
            }
            $addedMembers = array();
            $members = array();
            $startIterating = false;
            try{
                if(isset($_POST['_token'])){
                    $objPHPExcel = PHPExcel_IOFactory::load(Input::file('file'));
                    $loadedSheetNames = $objPHPExcel->getSheetNames();
                    foreach($loadedSheetNames as $sheetIndex => $loadedSheetName) {
                        $objPHPExcel->setActiveSheetIndex($sheetIndex);
                        $activeSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                        foreach($activeSheet as $row){

                            if($startIterating){
                                if(!($row["A"] == NULL)){
                                    $emriMbiemri =$row["B"];
                                    $emriMbiemri = explode(" ", $emriMbiemri);
                                    $member = new Member();
                                    $member->emri = $emriMbiemri[0];
                                    if(count($emriMbiemri) > 1)
                                        $member->mbiemri = $emriMbiemri[1];
                                    $member->datelindja = $row["C"];
                                    $member->telefoni = $row["F"];
                                    $member->profesioni = $row["H"];
                                    $member->num_personal       = $row["D"];
                                    $member->adresa             = $row["E"];
                                    $member->e_mail             = $row["G"];
                                    $member->data_anetaresimit  = date('Y-m-d');
                                    if(Auth::check())
                                        $member->admini             = Auth::user()->id;
                                    else
                                        $member->admini             = -1;
//                                $member->vendi_punes        = Input::get('employer');
//                                $member->kombesia           = Input::get('nationality');
                                    $m = DB::select('select * from members where num_personal = ?', array($member->num_personal));
                                    if($m == null){
                                        $member->save();
                                        $addedMembers[] = $member;
                                    }
                                    $members[] = $member;
                                }
                            }
                            $startIterating = ($startIterating || $row["A"] == "Nr.");
                        }
                    }
                }
                $count = count($addedMembers);
                if(0 < $count)
                    $countAddedMembersMESSAGE = 'U shtuan '.count($addedMembers).' anetare ne menyre te suksesshme';
                else
                    $countAddedMembersMESSAGE = 'Keta anetare tashme jane ne databaze, ose dokumenti nuk eshte ne formatin e duhur.';

                return Redirect::to('see/members')->with('addMemberConfirmation', $countAddedMembersMESSAGE);
            }catch (Exception $qe){
                return Redirect::to('see/members')->with('batchMembersError', 'Dicka nuk funksionoi si duhet. Sigurohu qe dokumenti te jete prezente dhe ne formatin e duhur');
            }
        }

        private function isAdmin(){
            return str_is(Auth::user()->username, 'admin');
        }
	}
?>