<?php
/**
 * Created by JetBrains PhpStorm.
 * User: gagi
 * Date: 2/2/14
 * Time: 2:33 AM
 * To change this template use File | Settings | File Templates.
 */

class MemberRegistration extends BaseController{

    public function register(){
        if(!isset($_POST['_token']))
            return View::make('members.addmembers');

        $view = View::make('members.addsuccess');
        $member = new Member;
        $member->emri = Input::get('firstname');
        $member->mbiemri = Input::get('lastname');
        $member->save();
        $added = (0 < $member->id);
        if($added){
            $view = $view->with('addMemberMessage', 'Anetari u shtua. Ju falenderojme per anetaresim.');
        }
        else{
            $view = $view->with('addMemberMessage', 'Anetari nuk u shtua. Luteni te provoni perseri.');
        }
        return $view;
    }
}