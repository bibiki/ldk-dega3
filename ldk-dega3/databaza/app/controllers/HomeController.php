<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
        $addedMembers = array();
        $members = array();
        $startIterating = false;
        if(isset($_POST['_token'])){
            $objPHPExcel = PHPExcel_IOFactory::load(Input::file('file'));
            $loadedSheetNames = $objPHPExcel->getSheetNames();
            foreach($loadedSheetNames as $sheetIndex => $loadedSheetName) {
                $objPHPExcel->setActiveSheetIndex($sheetIndex);
                $activeSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                foreach($activeSheet as $row){

                    if($startIterating){
                        if(!($row["A"] == NULL)){
                            $emriMbiemri =$row["B"];
                            $emriMbiemri = explode(" ", $emriMbiemri);
                            $member = new Member();
                            $member->emri = $emriMbiemri[0];
                            if(count($emriMbiemri) > 1)
                                $member->mbiemri = $emriMbiemri[1];
                            $member->datelindja = $row["C"];
                            $member->telefoni = $row["F"];
                            $member->profesioni = $row["H"];
                            $member->num_personal       = $row["D"];
                            $member->adresa             = $row["E"];
                            $member->e_mail             = $row["G"];
                            $member->data_anetaresimit  = date('Y-m-d');
                            if(Auth::check())
                                $member->admini             = Auth::user()->id;
                            else
                                $member->admini             = -1;
//                            $member->vendi_punes        = Input::get('employer');
//                            $member->kombesia           = Input::get('nationality');
                            $m = DB::select('select * from members where num_personal = ?', array($member->num_personal));
                            if($m == null){
                                $member->save();
                                $addedMembers[] = $member;
                            }
                            $members[] = $member;
//                            var_dump($row);
//                            echo "<br><br>";
                        }
                    }
                    $startIterating = ($startIterating || $row["A"] == "Nr.");
                }
                echo "<br>";
            }
        }
        $countAddedMembersMESSAGE = 'U shtuan '.count($addedMembers).' anetare ne menyre te suksesshme';
        return View::make('test.testform')->with('members', \Illuminate\Database\Eloquent\Collection::make($members));
    }
}