<?php

	class LoginController extends BaseController{
	
		public function logIn(){
            if(Auth::check())
                return Redirect::intended('see/members');
			$message = '';
			$view = View::make('login.login-form');
			if(isset($_POST['_token'])){
				if(
                    Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')))
                                    ||
                    Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')))
                ){
					//try logging in
                    if(!Auth::user()->is_active){
                        Auth::logout();
                        return $view->with('message', 'Ky user eshte jo aktiv.');
                    }
					return Redirect::to('see/members');
				}
				else{
					$view = $view->with('message', 'Username/email eshte gabim');
				}
			}
			return $view;
		}
		
		public function logOut(){
			if(Auth::check())
				Auth::logout();
			return View::make('login.login-form')->with('logout', 'U c\'lloguat me sukses!');
		}
	}
?>