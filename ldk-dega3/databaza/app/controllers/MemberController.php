<?php

	class MemberController extends BaseController{


	public function add(){
            $view = View::make('members.addmembers')->with('member', new Member());
            if(Auth::check()){
                $view = View::make('admin.addmembers')->with('member', new Member());
            }
	    $added = false;
	    $member = null;
	    if(isset($_POST['_token'])){
                $member = Member::firstOrCreate(array('num_personal' => Input::get('personalid')));
		$member->emri = Input::get('firstname');
		$member->mbiemri = Input::get('lastname');
                $member->datelindja         = Input::get('birthday');
                $member->num_personal       = Input::get('personalid');
                $member->grupi_gjakut       = Input::get('bloodtype');
                $member->adresa             = Input::get('address');
                $member->gjinia             = Input::get('gender');
                $member->profesioni         = Input::get('profession');
                $member->telefoni           = Input::get('phone');
                $member->dega               = Input::get('branch');
                $member->nendega            = Input::get('subbranch');
                $member->statusi_martesor   = Input::get('status');
                $member->e_mail             = Input::get('email');
                $member->data_anetaresimit  = date('Y-m-d');
                $member->i_punesuar        = Input::get('employed');
                if(Auth::check()){
                    $member->admini             = Auth::user()->id;
                }
                else
                    $member->admini             = -1;
                $member->vendi_punes        = Input::get('employer');
                $member->kombesia           = Input::get('nationality');
                $validator = $member->validate(Input::all());
                if($validator->fails()){
                    if(Auth::check()){
                        return Redirect::to('add/member')->withErrors($validator)->withInput(Input::all());
                    }
                    else{
                        return Redirect::to('/')->withErrors($validator)->withInput(Input::all());
                    }
                }
                try{
                    $member->save();
                    if(Auth::check()){
                        return Redirect::to('add/member')->with('addMemberConfirmation', 'Anetari u shtua');
                    }
                    else{
                        $data = array('emri' => 'Mentor', 'mbiemri' => 'Barani');
                        Mail::send('email.report', array('anetari' => $member), function($message) use ($member){
                            $message->from('admin@ldk-dega3.eu');
                            $message->to('info@ldk-dega3.eu');
                            $message->subject($member->emri. " " . $member->mbiemri . " u regjistrua.");
                        });
                        if(Input::has('email') && !str_is('', Input::get('email'))){
                        	Mail::send('email.thankyou', array('anetari' => $member), function($message) use ($member){
	                            $message->from('admin@ldk-dega3.eu');
	                            $message->to(trim(Input::get('email')));
	                	    $message->subject("Falemnderit për rejistrim ne degën e tretë të LDK-së, Prishtinë");
	                        });	
                        }
                        return Redirect::to('/')->with('addMemberConfirmation', 'Ju falemnderit për interesin tuaj në LDK! U regjistruat me sukses.');
                    }
                }catch (Exception $qe){
                    $member->admini = -1;
                    $view = $view->with('member', $member);
                    return $view;
                }
            }
			return $view;
        }

        public function see($id = null){
            if(is_null($id)){
                //THIS NEEDS TO BE REMOVED
                return View::make('admin.seemembers')->with('members', Member::take(1)->get());
            }
            else{
                $member = Member::find($id);
//                return View::make('admin.detailed_member')->with('member', $member);
                return View::make('admin.addmembers')->with('member', $member);
            }
        }
/*
 * This method is commented out and should be deleter completely when time comes.
 * Time comes though after the app has been live for at least one month.
        public function addBatchMembers(){
            $addedMembers = array();
            $members = array();
            $startIterating = false;
            if(isset($_POST['_token'])){
                $objPHPExcel = PHPExcel_IOFactory::load(Input::file('file'));
                $loadedSheetNames = $objPHPExcel->getSheetNames();
                foreach($loadedSheetNames as $sheetIndex => $loadedSheetName) {
                    $objPHPExcel->setActiveSheetIndex($sheetIndex);
                    $activeSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                    foreach($activeSheet as $row){

                        if($startIterating){
                            if(!($row["A"] == NULL)){
                                $emriMbiemri =$row["B"];
                                $emriMbiemri = explode(" ", $emriMbiemri);
                                $member = new Member();
                                $member->emri = $emriMbiemri[0];
                                if(count($emriMbiemri) > 1)
                                    $member->mbiemri = $emriMbiemri[1];
                                $member->datelindja = $row["C"];
                                $member->telefoni = $row["F"];
                                $member->profesioni = $row["H"];

                                $member->num_personal       = $row["D"];
                                $member->adresa             = $row["E"];
                                $member->e_mail             = $row["G"];
                                $member->data_anetaresimit  = date('Y-m-d');
                                if(Auth::check())
                                    $member->admini             = Auth::user()->id;
                                else
                                    $member->admini             = -1;
                                $member->vendi_punes        = Input::get('employer');
                                $member->kombesia           = Input::get('nationality');
                                $m = DB::select('select * from members where num_personal = ?', array($member->num_personal));
                                if($m == null){
                                    $member->save();
                                    $addedMembers[] = $member;
                                }
                                $members[] = $member;
//                            var_dump($row);
//                            echo "<br><br>";
                            }
                        }
                        $startIterating = ($startIterating || $row["A"] == "Nr.");
                    }
                }
            }
            $countAddedMembersMESSAGE = 'U shtuan '.count($addedMembers).' anetare ne menyre te suksesshme';
            return View::make('test.testform')->with('members', \Illuminate\Database\Eloquent\Collection::make($members));
        }*/

        /**
         * This is for returning members in JSON format
         */

        public function jsonMembers(){
//            $aaData = Member::all()->toArray();
            $queryBuilder = DB::table('members');
            if(Input::has('firstname') && !str_is('', Input::get('firstname'))){
                $queryBuilder->where('emri', 'like', '%'.Input::get('firstname').'%');
            }
            if(Input::has('lastname') && !str_is('', Input::get('lastname'))){
                $queryBuilder->where('mbiemri', 'like', '%'.Input::get('lastname').'%');
            }
            if(Input::has('personalid') && !str_is('', Input::get('personalid'))){
                $queryBuilder->where('num_personal', 'like', '%'.Input::get('personalid').'%');
            }
            if(Input::has('bloodtype') && !str_is('', Input::get('bloodtype'))){
                $queryBuilder->where('grupi_gjakut', 'like', '%'.Input::get('bloodtype').'%');
            }
            if(Input::has('profession') && !str_is('', Input::get('profession'))){
                $queryBuilder->where('profesioni', 'like', '%'.Input::get('profession').'%');
            }
            if(Input::has('employer') && !str_is('', Input::get('employer'))){
                $queryBuilder->where('vendi_punes', 'like', '%'.Input::get('employer').'%');
            }
            if(Input::has('phone') && !str_is('', Input::get('phone'))){
                $queryBuilder->where('telefoni', 'like', '%'.Input::get('phone').'%');
            }
            if(Input::has('email') && !str_is('', Input::get('email'))){
                $queryBuilder->where('e_mail', 'like', '%'.Input::get('email').'%');
            }
            
            if(Input::has('ethnicity') && !str_is('', Input::get('ethnicity'))){
                $queryBuilder->where('kombesia', 'like', '%'.Input::get('ethnicity').'%');
            }
            if(Input::has('gender') && !str_is('', Input::get('gender'))){
                $queryBuilder->where('gjinia', 'like', '%'.Input::get('gender').'%');
            }
            if(Input::has('branch') && !str_is('', Input::get('branch'))){
                $queryBuilder->where('dega', 'like', '%'.Input::get('branch').'%');
            }
            if(Input::has('subbranch') && !str_is('', Input::get('subbranch'))){
                $queryBuilder->where('nendega', 'like', '%'.Input::get('subbranch').'%');
            }
            if(Input::has('status') && !str_is('', Input::get('status'))){
                $queryBuilder->where('statusi_martesor', 'like', '%'.Input::get('status').'%');
            }
            if(Input::has('employed') && !str_is('', Input::get('employed'))){
                $queryBuilder->where('i_punesuar', 'like', '%'.Input::get('employed').'%');
            }
            if(Input::has('leftend') && !str_is('', Input::get('leftend'))){
                $queryBuilder->where('datelindja', '>=', Input::get('leftend'));
            }
            if(Input::has('rightend') && !str_is('', Input::get('rightend'))){
                $queryBuilder->where('datelindja', '<=', Input::get('rightend'));
            }
            $queryBuilder->orderBy('id', 'desc');
            $aaData = $queryBuilder->get();
            $totalRecords = count($aaData);
            $aaData =  $queryBuilder->skip($_GET['iDisplayStart'])->take($_GET['iDisplayLength'])->get();
            $response = array(
                'iTotalRecords' => $totalRecords,
                'iTotalDisplayRecords' => $totalRecords,
                'sEcho' => $_GET['sEcho'],
                'errorMessage' => null,
                'errorType' => null,
                'aaData' => $aaData
            );

            return $response;
        }

        public function delete($id){
            if($id != null){
                $member = Member::find($id);
                if(null != $member)
                    $member->delete();
            }
            return Redirect::intended('see/members');
        }

        public function downloadMembers(){
            $objPHPExcel = new PHPExcel();
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValue('A1', 'Emri');
            $sheet->setCellValue('B1', 'Mbiemri');
            $sheet->setCellValue('C1', 'Datelindja');
            $sheet->setCellValue('D1', 'Numri personal');
            $sheet->setCellValue('E1', 'Gjaku');
            $sheet->setCellValue('F1', 'Adresa');
            $sheet->setCellValue('G1', 'Gjinia');
            $sheet->setCellValue('H1', 'Profesioni');
            $sheet->setCellValue('I1', 'Telefoni');
            $sheet->setCellValue('J1', 'Dega');
            $sheet->setCellValue('K1', 'Nendega');
            $sheet->setCellValue('L1', 'Statusi Martesor');
            $sheet->setCellValue('M1', 'E mail');
            $sheet->setCellValue('N1', 'Kombesia');
            $sheet->setCellValue('O1', 'Punedhenesi');
            $starting_pos = ord('A');
            $row = 2;
            $members = Member::all()->toArray();
            foreach($members as $member){
                $index_pos = 0;
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["emri"]);
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["mbiemri"]);
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["datelindja"]);
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["num_personal"]);
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["grupi_gjakut"]);
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["adresa"]);
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["gjinia"]);
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["profesioni"]);
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["telefoni"]);
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["dega"]);
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["nendega"]);
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["statusi_martesor"]);
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["e_mail"]);
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["kombesia"]);
                $sheet->setCellValue(chr($starting_pos+$index_pos++) . $row, $member["vendi_punes"]);
                $row++;
            }
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            header('Content-Type: vnd.ms-excel');
            header('Content-Disposition: attachment; filename="anetaret.xls"');
            $objWriter->save('php://output');
        }
    }
