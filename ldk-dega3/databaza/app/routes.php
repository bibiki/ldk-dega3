<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
/*
    Route::get('/', function()
    {
        return View::make('hello');
    });
*/


Route::get('add/member', array('before' => 'auth', 'uses' => 'MemberController@add'));
Route::post('add/member', 'MemberController@add');
Route::get('delete/member/{id}', array('before' => 'auth', 'uses' => 'MemberController@delete'));
Route::get('see/members/{id}', array('before' => 'auth', 'uses' => 'MemberController@see'));
Route::get('see/members', array('before' => 'auth', 'uses' => 'MemberController@see'));
Route::get('get/members', array('before' => 'auth', 'uses' => 'MemberController@jsonMembers'));
Route::get('download', array('before' => 'auth', 'uses' => 'MemberController@downloadMembers'));

Route::get('/', 'MemberRegistration@register');

Route::get('/test', 'HomeController@showWelcome');
Route::post('/test', 'HomeController@showWelcome');
Route::get('login', 'LoginController@logIn');
Route::post('login', 'LoginController@logIn');
Route::get('logout', 'LoginController@logOut');

Route::get('add/user', array('before' => 'auth', 'uses' => 'UserController@add'));
Route::post('add/user', array('before' => 'auth', 'uses' => 'UserController@add'));
Route::post('update/user', array('before' => 'auth', 'uses' => 'UserController@update'));
Route::get('delete/user/{id}', array('before' => 'auth', 'uses' => 'UserController@delete'));
Route::get('see/users', array('before' => 'auth', 'uses' => 'UserController@seeUsers'));
Route::get('see/users/{id}', array('before' => 'auth', 'uses' => 'UserController@seeUsers'));
Route::get('user/settings', array('before' => 'auth', 'uses' => 'UserController@changeOwnSetting'));
Route::post('batch/members', array('before' => 'auth', 'uses' => 'UserController@batchMemberUpload'));


Route::get('remind', 'RemindersController@getRemind');
Route::post('remind', 'RemindersController@postRemind');
Route::get('password/reset/{token}', 'RemindersController@getReset');
Route::post('reset', 'RemindersController@postReset');

Route::get('register', 'MemberRegistration@register');
Route::post('register', 'MemberRegistration@register');

App::missing(function($exception){
    return View::make('members.404');
    //return View::make('members.under_construction');

});