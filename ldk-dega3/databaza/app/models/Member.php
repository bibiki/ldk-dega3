<?php

	class Member extends Eloquent{
		public $timestamps = false;
        //public $gjinia = 'femer';
        protected $fillable = array('num_personal');

        private $validationErrors;
        private $messages = array(
            'required' => 'Fusha :attribute eshte e domosdoshme',
            'digits'    => 'Fusha :attribute duhet qene saktesisht :digits numra.',
            'between' => 'Fusha :attribute duhet qene se paku :min dhe jo me shume se :max karaktere.',
            'in'      => 'Fusha :attribute duhet qene njera nga keto: :values',
            'date_format' => 'Datëlindja nuk përkon me formatin:mm/dd/vvvv'
        );


        private $attributeNames = array(
            'firstname' => 'Emri',
            'lastname' => 'Mbiemri',
            'birthday' => 'Datelindja',
            'personalid' => 'Numri personal',
            'bloodtype' => 'Grupi gjakut',
            'adresa' => 'Adresa',
            'gender' => 'Gjinia',
            'profession' => 'Profesioni',
            'telefoni' => 'Telefoni',
            'dega' => 'Dega',
            'nendega' => 'Nendega',
            'status' => 'Statusi martesor',
            'e_mail' => 'Email',
            'kombesia' => 'Kombesia',
            'vendi_punes' => 'Vendi i punes',
            'i_punesuar' => 'I punesuar'
        );
        public function getValidationErrors(){
            return $this->validationErrors;
        }

        public function validate($input){
            $rules = array(
                'firstname' => 'Required|AlphaNum|Between:3,20',
                'lastname' => 'Required|AlphaNum|Between:3,20',
                'birthday' => 'Required|date_format:"Y/m/d"',
                'personalid' => 'Required|digits:10',
                'bloodtype' => 'in:,0+,0-,A+,A-,B+,B-,AB+,AB-',
//                'adresa' => '',
                'gender' => 'in:mashkull,femer',
                'profession' => 'Alpha',
                /*'telefoni' => '',*/
                'dega' => 'AlphaNum',
                'nendega' => 'AlphaNum',
                'status' => 'in:i/e martuar,i/e pamartuar',
                'e_mail' => 'Between:3,64|Email',
                'kombesia' => 'Alpha',
                'vendi_punes' => 'AlphaNum',
                'i_punesuar' => 'in:Po,Jo'
            );

            $input = static::arrayStripTags($input);
            $v = Validator::make($input, $rules, $this->messages);
            $v->setAttributeNames($this->attributeNames);
            if($v->fails()){
                $this->validationErrors = $v->errors();
            }
            return $v;

        }
        /*This is my server side xss protection. It strips any tags away from input.*/
        private static function arrayStripTags($array)
        {
            $result = array();

            foreach ($array as $key => $value) {
                // Don't allow tags on key either, maybe useful for dynamic forms.
                $key = strip_tags($key);

                // If the value is an array, we will just recurse back into the
                // function to keep stripping the tags out of the array,
                // otherwise we will set the stripped value.
                if (is_array($value)) {
                    $result[$key] = static::arrayStripTags($value);
                } else {
                    // I am using strip_tags(), you may use htmlentities(),
                    // also I am doing trim() here, you may remove it, if you wish.
                    $result[$key] = trim(strip_tags($value));
                }
            }

            return $result;
        }
	}
?>