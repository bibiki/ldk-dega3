<?php

use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('members', function($table){
			$table->increments('id');
			$table->string('emri', 16);
			$table->string('mbiemri', 16);
			$table->dateTime('datelindja');
			$table->string('num_personal', 10);
			$table->enum('grupi_gjakut', array('A', 'B', '0'));//POPULATE THE ARRAY
			$table->string('adresa', 45);
			$table->enum('gjinia', array('Mashkull', 'Femer'));
			$table->string('profesioni', 20);
			$table->string('telefoni', 16);
			$table->string('dega');
			$table->string('nendega', 16);
			$table->string('statusi_martesor');//consider making enum
			$table->string('e_mail', 50);
			$table->dateTime('data_anetaresimit');//should it be year, month, or both?
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}