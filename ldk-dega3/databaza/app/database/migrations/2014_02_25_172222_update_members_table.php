<?php

use Illuminate\Database\Migrations\Migration;

class UpdateMembersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('members', function($table){
            $table->string('kombesia');
            $table->integer('admini');
            $table->string('vendi_punes');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}