<?php get_header(); ?>

<!-- - - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->	

<!--this is page.php-->	

	<section class="container sbr clearfix">

		

		<!-- - - - - - - - - - Page Header - - - - - - - - - - -->	

		

		<div class="page-header">

			

			<h1 class="page-title"><?php the_title();?></h1>

			

		</div><!--/ .page-header-->

		

		<!-- - - - - - - - - end Page Header - - - - - - - - - -->	

		



		<!-- - - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->		

		

		<section id="content" class="first">



			
		<?php if (have_posts()) : ?>
          <?php while (have_posts()) : the_post();?>
       


		<!-- - - - - - - - - - - - Paragraph with images - - - - - - - - - - - -->	





   			<div class="img-content">
				<?php the_content() ?>
			</div>

			

		</section><!--/ #content-->

		  <?php endwhile;?>
          <?php endif;?>

		<!-- - - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - - -->	

		

		

		<!-- - - - - - - - - - - - - - - Sidebar - - - - - - - - - - - - - - - - -->	


<?php get_sidebar(); ?>	
<?php get_footer(); ?>