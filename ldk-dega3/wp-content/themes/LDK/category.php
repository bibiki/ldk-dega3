<?php get_header(); ?>


<!-- this is category -->

	

	<!-- - - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->	

	

	<section class="container sbr clearfix">

		

		<!-- - - - - - - - - - Page Header - - - - - - - - - - -->	

		

		<div class="page-header">

			

			<h1 class="page-title"><?php single_cat_title(); ?></h1>

			

		</div><!--/ .page-header-->

		

		<!-- - - - - - - - - end Page Header - - - - - - - - - -->	

		



		<!-- - - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->		

		

		<section id="content" class="first">

			

			<article class="post clearfix">

				<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>		
				<br>
				<a href="<?php the_permalink(); ?>">

					<h3 class="title">

						<?php the_title(); ?>

					</h3><!--/ .title -->

				</a>

				


				

				<a class="single-image" href="<?php the_permalink();?>">

					<?php the_post_thumbnail(array(628,400),array('class' => 'custom-frame news-img') );?>

				</a>

				

				<p>

					<?php the_excerpt(); ?>

				</p>

				

				<a href="<?php the_permalink(); ?>" class="button gray">Lexo më shumë &rarr;</a>

				<div class="post-bottom"></div>

			</article><!--/ .post-->
	
		<?php endwhile; ?>

		</section><!--/ #content-->

		

		<!-- - - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - - -->	

		
		<?php get_sidebar(); ?>	
		



		

		<ul class="pagination">

			

<?php posts_nav_link(); ?>

			

		</ul><!--/ .pagination-->

		

	<?php endif; ?>	

	</section><!--/.container -->

		

	<!-- - - - - - - - - - - - - end Container - - - - - - - - - - - - - - - - -->	

	

	





<?php get_footer(); ?>