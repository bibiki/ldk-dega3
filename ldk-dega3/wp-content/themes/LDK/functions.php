<?php


add_image_size('thumb-img-size', 268, 202);
//Some simple code for our widget-enabled sidebar
if ( function_exists('register_sidebar') )
    register_sidebar();

//Add support for WordPress 3.0's custom menus
add_action( 'init', 'register_my_menu' );

//Register area for custom menu
function register_my_menu() {
	register_nav_menu( 'primary-menu', __( 'Primary Menu' ) );
}

//Code for custom background support
add_custom_background();

//Enable post and comments RSS feed links to head
add_theme_support( 'automatic-feed-links' );

// Enable post thumbnails
add_theme_support('post-thumbnails');
set_post_thumbnail_size(628, 325);

function register_my_menus() {
  register_nav_menus(
    array( 'header-menu' => __( 'Header Menu' ) )
  );
}
add_action( 'init', 'register_my_menus' );




function custom_wp_trim_excerpt($text) {
	$raw_excerpt = $text;
	
	if ( '' == $text ) {
		//Retrieve the post content.
		$text = get_the_content('');

		//Delete all shortcode tags from the content.
		$text = strip_shortcodes( $text );
	 
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]&gt;', $text);
		
		$allowed_tags = '<p>,<br>, <a>,<em>'; //,<strong>
		$text = strip_tags($text, $allowed_tags);
		
		$excerpt_end = '...'; /*** MODIFY THIS. change the excerpt endind to something else.***/
		if(strlen($text) > strpos($text, "-") + 220)
			$text = substr($text, 0, strpos($text, "-") + 420)." ".$excerpt_end;		

		$excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end);
		 
		//echo "baba: ".substr_count ( $text, "</p>");
		 
		$excerpt_word_count = 40; /*** MODIFY THIS. change the excerpt word count to any integer you like.***/
		$excerpt_length = apply_filters('excerpt_length', $excerpt_word_count);
		 
		$text = str_replace('</p>', '', $text);
		$text = str_replace('<p>', '', $text);
		
		$words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
		if ( count($words) > $excerpt_length ) {
			array_pop($words);
			$text = implode(' ', $words);
			$text = $text . $excerpt_more;
		} else {
			$text = implode(' ', $words);
		}
		
		if(strlen($text) > 220)
		//	$text = substr($text, 0, 430);
		$text = str_replace("<p>", "<br>", $text);
	}
	$text = apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
	if(strpos($text, "</a> <br>") > 0){
			$text = str_replace("</a> <br>", "</a>", $text);
	}
	return $text;
}
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'custom_wp_trim_excerpt');


?>