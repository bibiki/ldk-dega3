<!DOCTYPE html>

<!--[if IE 7]>					<html class="ie7 no-js" lang="en">     <![endif]-->

<!--[if lte IE 8]>              <html class="ie8 no-js" lang="en">     <![endif]-->

<!--[if (gte IE 9)|!(IE)]><!--> <html class="not-ie no-js" lang="en">  <!--<![endif]-->

<head>

	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">



	<title>LDK - Dega III</title>



	<meta name="description" content="">

	<meta name="author" content="">



	<link rel="shortcut icon" href="wp-content/themes/LDK/favicon.ico"/>



	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/sliders/flexslider/flexslider.css" />

	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/fancybox/jquery.fancybox.css" />
	

	<!-- HTML5 Shiv -->

	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/modernizr.custom.js"></script>
	
<?php wp_head(); ?>

</head>

<body class="style-1">



<div class="wrap-header"></div><!--/ .wrap-header-->




<div class="wrap">


	<!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

<header id="header" class="clearfix">
	
<!-- <h1>share</h1> -->

		<a href="<?php home_url();?>" id="logo"><img style="width: 100%" src="<?php bloginfo('template_directory'); ?>/images/logo.png" title="LDK DEGA III" alt="LDK" /></a>









		<nav id="navigation" class="navigation">



			<?php wp_nav_menu( array( 'container' => 'ul' ) ); ?>
		</nav><!--/ #navigation-->



	</header><!--/ #header-->



	<!-- - - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - - -->