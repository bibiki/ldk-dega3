
/* Page config --> Begin */

var page_config = {
   style : {
        1 : {
            name : 'Style 1',
            className : 'style-1'
        },
        2 : {
            name : 'Style 2',
            className : 'style-2'
        },
        3 : {
            name : 'Style 3',
            className : 'style-3'
        },
        4 : {
            name : 'Style 4',
            className : 'style-4'
        },
        5 : {
            name : 'Style 5',
            className : 'style-5'
        },
        6 : {
            name : 'Style 6',
            className : 'style-6'
        }
    }
}

/* Page config --> End */

jQuery(document).ready(function($) {


		/* Style --> Begin */
		
        if (page_config.style) {
            var $bg_block = $('<div/>').attr('id','style').addClass('style');
			var bg_change_html = '';
			bg_change_html += '<ul>';
            $.each(page_config.style, function(idx, val) {
				
                bg_change_html += '<li><a href="#" title="' + val.name + '" class="' + val.className + '"></a></li>';
                styleArray.push(val.className);
				
            });
            bg_change_html += '</ul>';
            $bg_block.html(bg_change_html);
            $theme_control_panel.append($bg_block);
			
            $bg_block.find('a').click(function() {
                var nextClassName = $(this).attr('class');
				
				jQuery.cookie('style', nextClassName);
				
                if (!$body.hasClass(nextClassName)) {
                    changeBodyClass(nextClassName, styleArray);
                    $bg_block.find('.active').removeClass('active');
                    $(this).parent().addClass('active');
				}
				
                return false;
            });
        }

		/* Style --> End */
		
		$('.wrap').after($theme_control_panel);

    }

    /* Theme controller --> End */

});
