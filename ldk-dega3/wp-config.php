<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/home/ldkdega3/public_html/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'bibiki_dega3');

/** MySQL database username */
define('DB_USER', 'bibiki_ldk');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'c_18TM1JK<>t)Z^c8(Tayr@tvmH3c~i>M)e5x9>_1~5-@veTAaP<c(E)7qj/Rbs^>1hFN436_Xg451oGn<');
define('SECURE_AUTH_KEY',  'h5LotYR~88hqh\`moH;lg4QNMv/r\`pBE<rm*0H*)FYvP7Zzt<4~k(CJJ#WScWXgjwwKY0\`yh)_RBa');
define('LOGGED_IN_KEY',    'lSn!p:?/6qIB7#4wI@^s$_wY_(bDPDyX:9q_hsL(Kh8)EAFA30U1oOr(Gny!qusm>e0LLpWk3Oy@XsbR\`5');
define('NONCE_KEY',        'GDu*UXpx#_VVBxdrOuMINP$?52r)Gc:W#FxBb_/G:5~eZH~h)DmP|0lYII)nq(P4v07');
define('AUTH_SALT',        '?4WsD3nkuWyn_6:sP!S/0LrPiU0u\`!2)9~L|/TYeo^(*fKoXZ!d7Do)K1T;VI8q(jd!O~?PbkD');
define('SECURE_AUTH_SALT', 'sS~(:O8K(e3*q_kqlqdZ!af40@AX!/dtLiM1!mW5vWH~bMS8D5mvqVLO;LbVT=;oSwx3s');
define('LOGGED_IN_SALT',   'ZpaAyv>m<m*4Ty)~De8A5@UsgbQnJ$zRO8DdsV0v\`>vnbGT)hxRKOjkY*nuD!');
define('NONCE_SALT',       '1|1=W(Jz8T4@u)4K871$v_<sbU(b5ynxi(0S8=z;20#u;PYUv\`fTQ1<py:omvkuC');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
